//
//  MarvelTests.swift
//  MarvelTests
//
//  Created by Guilherme Antunes on 24/06/17.
//  Copyright © 2017 Guihsoft. All rights reserved.
//

import XCTest
@testable import Marvel

class MarvelTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testOrdering() {
        
        let heroesList = HeroesListViewController()
        heroesList.orderByAsc = true
        
        var hero1 = Hero()
        var hero2 = Hero()
        var hero3 = Hero()
        var hero4 = Hero()
        
        hero1.name = "Ben Parker"
        hero2.name = "Spectacular Spiderman"
        hero3.name = "Invencible Ironman"
        hero4.name = "Thor"
        hero4.starred = true
        
        var array = [Hero]()
        array.append(hero4)
        array.append(hero1)
        array.append(hero3)
        array.append(hero2)

        let array2 = heroesList.orderTheChars(hArray: array)
        
        let array3 = [hero4, hero1, hero3, hero2]
        
        for i in 0...(array2.count - 1) {
            
            let name1 = array2[i].name
            let name2 = array3[i].name
            
            XCTAssertEqual(name1, name2)
            
        }
        
        heroesList.orderByAsc = false
        
        let array4 = [hero4, hero2, hero3, hero1]
        
        let array5 = heroesList.orderTheChars(hArray: array)
        
        for i in 0...(array5.count - 1) {
            
            let name1 = array5[i].name
            let name2 = array4[i].name
            
            XCTAssertEqual(name1, name2)
            
        }
        
    }
    
}
