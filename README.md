# README #

1. Para executar o código, basta abrir o arquivo com extensão .xworkspace e teclar CMD + R.
2. Foram utilizadas algumas third-party libraries para agilizar o desenvolvimento do app.
3. Em alguns pontos do app, não foram consideradas melhores soluções para perfomance e armazenamento, mas a solução funciona para o propósito dela.
4. O serviço da Marvel limita a busca de 100 heróis por requisição, por isso, não são 100% dos heróis que estarão presentes ao executar o aplicativo, mas pode-se alterar os heróis que serão retornados mudando alguns parâmetros no código como o "Offset" no dicionário da requisição GET da primeira tela (HeroesListViewController).
5. Foi utilizado o padrão de arquitetura MVC para desenvolvimento do projeto.
6. Há presença de testes unitários para algumas funcionalidade do aplicativo, porém, acredito que o desenvolvimento de testes é uma prática contínua no desenvolvimento do aplicativo e não há uma quantidade certa de testes a serem feitos, mas sim, quanto mais testes que cubram mais funcionalidades e casos, melhor.

Aproveitem.

Muito obrigado.

Antunes, Guilherme.