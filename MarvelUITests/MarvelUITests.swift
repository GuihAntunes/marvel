//
//  MarvelUITests.swift
//  MarvelUITests
//
//  Created by Guilherme Antunes on 24/06/17.
//  Copyright © 2017 Guihsoft. All rights reserved.
//

import XCTest

class MarvelUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testAppFlow() {
        
//      MARK: - Test Properties
        let app = XCUIApplication()
        let ordenarButton = app.navigationBars["Heroes List"].buttons["Ordenar"]
        let qtdRows = app.tables.tableRows.count
        let tablesQuery = app.tables
        
//      Initiating tests
        
        app.launch()
        
        XCTAssertTrue(app.tables.count == 1)
        
        XCTAssertTrue(app.buttons.count == (qtdRows + 1))
        
        ordenarButton.tap()
        ordenarButton.tap()
        ordenarButton.tap()
        ordenarButton.tap()
        
        tablesQuery.cells.containing(.staticText, identifier:"Captain Marvel (Genis-Vell)").buttons["✭"].tap()
        ordenarButton.tap()
        ordenarButton.tap()

        app.tables.cells.staticTexts["Ben Parker"].tap()
        app.navigationBars["Hero Detail"].buttons["Back"].tap()
 
        
    }
    
}
