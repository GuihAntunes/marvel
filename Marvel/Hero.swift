//
//  Hero.swift
//  Marvel
//
//  Created by Guilherme Antunes on 24/06/17.
//  Copyright © 2017 Guihsoft. All rights reserved.
//

import Foundation
import Freddy

struct Hero {

    var id : Int = 0
    var imageUrl : String = ""
    var imageExtension : String = ""
    var name : String = ""
    var starred : Bool = false
    var details : String = ""
    var image : UIImage = UIImage(contentsOfFile: Bundle.main.path(forResource: "image_not_available", ofType: "jpg")!)!
    
    init() {
        
    }
    
}

extension Hero : JSONDecodable {
    init(json value: JSON) throws {
        self.id = try value.getInt(at: "id")
        self.imageUrl = try value.getString(at: "thumbnail","path")
        self.name = try value.getString(at: "name")
        self.imageExtension = try value.getString(at: "thumbnail","extension")
        self.details = try value.getString(at: "description")
    }
}

extension Hero {
    
    static func download(hero : Hero) {
        
        let heroToSaveName = hero.name
        let heroToSaveDetails = hero.details

        
        let filePathName = (pathDocuments as NSString).appendingPathComponent("\(heroToSaveName).txt")
        let filePathDetails = (pathDocuments as NSString).appendingPathComponent("\(heroToSaveName)Details.txt")
        
        heroesSavedName.append(filePathName)
        heroesSavedDetails.append(filePathDetails)
        
        do {
            try heroToSaveName.write(toFile: filePathName, atomically: true, encoding: .utf8)
            try heroToSaveDetails.write(toFile: filePathDetails, atomically: true, encoding: .utf8)
        }catch{print("Error on saving!")}
        
    }
    
    static func downloadImage(hero : Hero) {
        if let heroToSaveImageData = UIImagePNGRepresentation(hero.image) {
            let filePathImage = (pathDocuments as NSString).appendingPathComponent("\(hero.name).png")
            let url = URL(fileURLWithPath: filePathImage)
            try? heroToSaveImageData.write(to: url, options: Data.WritingOptions.atomic)
            heroesSavedImage.append(url)
        }
    }
    
    static func delete(heroName : String) {
       
        let filePathName = (pathDocuments as NSString).appendingPathComponent("\(heroName).txt")
        let filePathDetails = (pathDocuments as NSString).appendingPathComponent("\(heroName)Details.txt")
        let filePathImage = (pathDocuments as NSString).appendingPathComponent("\(heroName).png")
        let url = URL(fileURLWithPath: filePathImage)
        
        do {
            try fileMan.removeItem(atPath: filePathDetails)
            try fileMan.removeItem(atPath: filePathName)
            try fileMan.removeItem(at: url)
            
            if let heroDeleted = heroesSavedName.index(of: "\(heroName).txt") {
                heroesSavedName.remove(at: heroDeleted)
                heroesSavedDetails.remove(at: heroDeleted)
                heroesSavedName.remove(at: heroDeleted)
            }
            
        }catch{print("Error on deleting!")}
        
    }
    
}
