//
//  HeroDetailViewController.swift
//  Marvel
//
//  Created by Guilherme Antunes on 25/06/17.
//  Copyright © 2017 Guihsoft. All rights reserved.
//

import UIKit
import SwiftHTTP
import Freddy
import CoreData

class HeroDetailViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var heroImage: UIImageView!
    @IBOutlet weak var detailText: UITextView!
    @IBOutlet weak var eventsGallery: UICollectionView!
    @IBOutlet weak var favoriteButton: UIButton!
    
    // MARK: - Properties
    let eventsQtd = 100
    let filePath = Bundle.main.path(forResource: "image_not_available", ofType: "jpg")!
    var favorited = false
    var eventsImages = [UIImage]()
    var offlineEvents = Event()
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.eventsGallery.dataSource = self
        self.eventsGallery.delegate = self
        self.setup()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        let indexOfHero = heroesArray.index { (hero) -> Bool in
            hero.id == selectedHeroId
        }

        guard indexOfHero != nil else {
            return
        }
        
        switch heroesArray[indexOfHero!].starred {
        case true:
            self.favoriteButton.setTitle("⭐️", for: UIControlState())
            self.favorited = true
            
        case false:
            self.favoriteButton.setTitle("✭", for: UIControlState())
            self.favorited = false
       }
        
        
        let publicKey = "778a229f3983f75f191d735a52a4ebd6"
        let privateKey = "fc1a0bc3cdf184d122b6d73b0e2a1bb99ab478c5"
        let ts = Date().timeIntervalSince1970.description
        let hash = "\(ts)\(privateKey)\(publicKey)".md5()
        let eventsUrl = "http://gateway.marvel.com/v1/public/characters/\(selectedHeroId)/events"
        
        do{
            
            let result = try HTTP.GET(eventsUrl, parameters: ["limit" : self.eventsQtd, "apikey" : publicKey, "ts" : ts, "hash" : hash!])
            
            result.start({ (response) in
                guard response.error == nil else {
                    print(response.error!.localizedDescription)
                    return
                }
                
                do{
                    let json = try JSON(data: response.data)
                    
                    //                    print(json)
                    
                    eventsArray = try json.getArray(at: "data","results").map(Event.init)
                    
                    DispatchQueue.main.async {
                        
                        stopLoading()
                        self.eventsGallery.reloadData()
                        if eventsArray.isEmpty {
                            let alert = UIAlertController(title: "Ops!", message: "No events found for this character!", preferredStyle: .alert)
                            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                            
                            alert.addAction(okAction)
                            self.present(alert, animated: true, completion: nil)
                        }
                    }

                }catch{print("Error on parse")}
            })
            
            startLoading(view: self.view)
            
        }catch{print("Error on request")}
        
    }
    
    // MARK: - General Methods
    
    func setup() {
        
        let indexOfHero = heroesArray.index { (hero) -> Bool in
            hero.id == selectedHeroId
        }

        guard indexOfHero != nil else{
            print("Hero not found!")
            return
        }

        if heroesArray[indexOfHero!].details == "" {
            self.detailText.text = "No description available!"
        }else{
            self.detailText.text = heroesArray[indexOfHero!].details
        }
        
        let urlImage = heroesArray[indexOfHero!].imageUrl + "." + heroesArray[indexOfHero!].imageExtension
        
        do{
            let request = try HTTP.Download(urlImage) { (url) in
                let data = try? Data(contentsOf: url)
                
                guard data != nil else{
                    self.heroImage.image = UIImage(contentsOfFile: self.filePath)
                    return
                }

                
                let image = UIImage(data: data!)
                DispatchQueue.main.async {
                    self.heroImage.image = image
                    heroesArray[indexOfHero!].image = image!
                    stopLoading()
                }
            }
            
            request.start()
            
            startLoading(view: self.view)
            
        }catch let error {print(error.localizedDescription)}
        
    }
    
    func getEventsFromDevice() {
        
        var newEvent = Event()
        
        for i in 0...(eventsSavedImages.count - 1) {
            
            guard eventsSavedImages[heroesSavedName[i]] != nil else {
                return
            }
            
            guard let count = eventsSavedImages[heroesSavedName[i]]?.count else {
                return
            }
            
            for j in 0...(count - 1) {
                guard let string = try? String(contentsOf: (eventsSavedImages[heroesSavedName[i]]?[j])!) else {
                    return
                }
                
                newEvent.images.append(UIImage(contentsOfFile: string)!)
            }
            
        }
        
        self.offlineEvents = newEvent
        
    }
    
    // MARK: - Actions
    @IBAction func saveHero() {
        
        let indexOfHero = heroesArray.index { (hero) -> Bool in
            hero.id == selectedHeroId
        }
        
        guard indexOfHero != nil else {
            print("Hero not found!")
            return
        }
        
        switch self.favorited {
        case false:
            self.favoriteButton.setTitle("⭐️", for: UIControlState())
            self.favorited = true
            heroesArray[indexOfHero!].starred = true
            Hero.download(hero: heroesArray[indexOfHero!])
            Hero.downloadImage(hero: heroesArray[indexOfHero!])
            Event.saveEvents(images: self.eventsImages, heroName: heroesArray[indexOfHero!].name)

        case true:
            self.favoriteButton.setTitle("✭", for: UIControlState())
            self.favorited = false
            heroesArray[indexOfHero!].starred = false
            Hero.delete(heroName: heroesArray[indexOfHero!].name)
            Event.deleteEvents(heroName: heroesArray[indexOfHero!].name)
        }
        
    }

}

// MARK: - Collection View DataSource and Delegate Methods

extension HeroDetailViewController : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return eventsArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "eventCell", for: indexPath) as! EventCollectionViewCell
        
        let url = eventsArray[indexPath.item].path + "." + eventsArray[indexPath.item].ext
        
        do{
            
            let request = try HTTP.Download(url, completion: { (url) in
                guard let data = try? Data(contentsOf: url) else{
                    cell.eventImage.image = UIImage(contentsOfFile: self.filePath)
                    return
                }
                let image = UIImage(data: data)
                self.eventsImages.append(image!)
                
                DispatchQueue.main.async {
                    cell.eventImage.image = image
                    stopLoading()
                }
                
                
            })
            
            request.start()
            startLoading(view: self.view)
            
        }catch{
            print("Error on request!")
            if eventsArray.isEmpty {
                self.getEventsFromDevice()
                
                cell.eventImage.image = self.offlineEvents.images[indexPath.item]
                
            }
        }
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width / 3 - 1
        
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
}
