//
//  Globals.swift
//  Marvel
//
//  Created by Guilherme Antunes on 25/06/17.
//  Copyright © 2017 Guihsoft. All rights reserved.
//

import Foundation
import UIKit

let showActivity = UIActivityIndicatorView()
let sandBoxPath = NSHomeDirectory()
let pathDocuments = (sandBoxPath as NSString).appendingPathComponent("Documents")
let fileMan = FileManager.default
var heroesSavedName = [String]()
var heroesSavedDetails = [String]()
var heroesSavedImage = [URL]()
var eventsSavedImages = [String:[URL]]()

func startLoading(view : UIView) {
    
    showActivity.center = CGPoint(x: view.center.x, y: view.center.y)
    showActivity.color = createColor(red: 110, green: 43, blue: 119)
    view.addSubview(showActivity)
    showActivity.startAnimating()
    
}

func stopLoading() {
    
    showActivity.stopAnimating()
    showActivity.removeFromSuperview()
    
}

func createColor(red : CGFloat, green : CGFloat, blue : CGFloat) -> UIColor {
    
    let red = validateColor(num: CGFloat(red))
    let green = validateColor(num: CGFloat(green))
    let blue = validateColor(num: CGFloat(blue))
    
    return UIColor(red: red, green: green, blue: blue, alpha: 1.0)
    
}

func validateColor(num : CGFloat) -> CGFloat{
    let result = num.truncatingRemainder(dividingBy: CGFloat(256))
    return result/255
}
