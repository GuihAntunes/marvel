//
//  HeroesListTableViewCell.swift
//  Marvel
//
//  Created by Guilherme Antunes on 24/06/17.
//  Copyright © 2017 Guihsoft. All rights reserved.
//

import UIKit

class HeroesListTableViewCell: UITableViewCell {

    // MARK: - Outlets
    
    @IBOutlet weak var heroName: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!
    
    // MARK: - Properties
    
    var favorited = false
    
    // MARK: - General Methods
    
    // MARK: - Actions
    @IBAction func saveHero() {
        
        // Download the hero
        
        let indexOfHero = heroesArray.index { (hero) -> Bool in
            hero.name == self.heroName.text
        }
        
        guard indexOfHero != nil else {
            print("Hero not found!")
            return
        }
        
        switch self.favorited {
        case false:
            self.favoriteButton.setTitle("⭐️", for: UIControlState())
            self.favorited = true
            heroesArray[indexOfHero!].starred = true
            Hero.download(hero: heroesArray[indexOfHero!])
        case true:
            self.favoriteButton.setTitle("✭", for: UIControlState())
            self.favorited = false
            heroesArray[indexOfHero!].starred = false
            Hero.delete(heroName: heroesArray[indexOfHero!].name)
        }
        
    }

}
