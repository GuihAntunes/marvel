//
//  HeroesListViewController.swift
//  Marvel
//
//  Created by Guilherme Antunes on 24/06/17.
//  Copyright © 2017 Guihsoft. All rights reserved.
//

import UIKit
import SwiftHTTP
import Freddy
import CoreData

class HeroesListViewController: UIViewController {

    // MARK: - Outlets
    @IBOutlet weak var heroTableView: UITableView!
    
    @IBOutlet weak var searchBarOutlet: UISearchBar!
    // MARK: - Properties
    
    let charsQtd = 100
    var searchResults : [Hero] = []
    var searchActive : Bool = false
    var orderByAsc : Bool = false
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        heroTableView.delegate = self
        heroTableView.dataSource = self
        searchBarOutlet.delegate = self
        self.downloadChars()
   
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.heroTableView.reloadData()
    }
    
    // MARK: - General Methods
    
    func getCharsFromDevice() {
        
        var newHero = Hero()
        
        for i in 0...(heroesSavedName.count - 1) {
            
            newHero.name = heroesSavedName[i]
            newHero.details = heroesSavedDetails[i]
            heroesArray.append(newHero)
        }
        
    }
    
    func downloadChars() {
        
        let baseUrl = "http://gateway.marvel.com/v1/public/characters"
        let publicKey = "778a229f3983f75f191d735a52a4ebd6"
        let privateKey = "fc1a0bc3cdf184d122b6d73b0e2a1bb99ab478c5"
        let ts = Date().timeIntervalSince1970.description
        let hash = "\(ts)\(privateKey)\(publicKey)".md5()
        
        do{
            
            let result = try HTTP.GET(baseUrl, parameters: ["limit" : self.charsQtd, "offset" : 100, "apikey" : publicKey, "ts" : ts, "hash" : hash!])
            
            result.start({ (response) in
                guard response.error == nil else {
                    print(response.error!.localizedDescription)
                    return
                }
                
                do{
                    let json = try JSON(data: response.data)
                    
//                    print(json)
                    
                    heroesArray = try json.getArray(at: "data","results").map(Hero.init)
                    
//                    print(heroesArray)
                    
                    DispatchQueue.main.async {
                        self.heroTableView.reloadData()
                        stopLoading()
                    }
                    
                    
                    
                }catch{print("Error on parse")}
                
            })
            
            startLoading(view: self.view)
            
        }catch{
            print("Error on request")
            if heroesArray.isEmpty {
                self.getCharsFromDevice()
            }
        }
    }
    
    func orderTheChars(hArray : [Hero]) -> [Hero] {
        
        var resultArray = [Hero]()
        
        let starred = hArray.filter { (hero) -> Bool in
            hero.starred == true
        }
        
        let nonStarred = hArray.filter { (hero) -> Bool in
            hero.starred == false
        }
        
        resultArray.append(contentsOf: starred)
        
        if self.orderByAsc {
            let restOfHeroes = nonStarred.sorted { (hero1, hero2) -> Bool in
                hero1.name < hero2.name
            }
            self.orderByAsc = false
            resultArray.append(contentsOf: restOfHeroes)
            
        }else{
            let restOfHeroes = nonStarred.sorted { (hero1, hero2) -> Bool in
                hero2.name < hero1.name
            }
            self.orderByAsc = true
            resultArray.append(contentsOf: restOfHeroes)
        }
        
        return resultArray
        
    }
    
    // MARK: - Actions
    @IBAction func orderTable(_ sender: UIBarButtonItem) {
        
        heroesArray = self.orderTheChars(hArray: heroesArray)
        self.heroTableView.reloadData()
        
    }

}

// MARK: - Tableview DataSource and Delegate Protocols

extension HeroesListViewController : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.searchActive {
            return searchResults.count
        }else{
            return heroesArray.count
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "heroCell", for: indexPath) as! HeroesListTableViewCell
        

        if self.searchActive {
            cell.heroName.text = searchResults[indexPath.row].name
        }else{
            cell.heroName.text = heroesArray[indexPath.row].name
        }
        
        if heroesArray[indexPath.row].starred {
            cell.favorited = true
            cell.favoriteButton.setTitle("⭐️", for: UIControlState())
        }else{
            cell.favorited = false
            cell.favoriteButton.setTitle("✭", for: UIControlState())
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.searchBarOutlet.text = ""
        
        if self.searchActive {
            self.searchBarOutlet.resignFirstResponder()
            selectedHeroId = searchResults[indexPath.row].id
        }else{
            selectedHeroId = heroesArray[indexPath.row].id
        }
    }
    
}

// MARK: - SearchBar Delegate Protocols

extension HeroesListViewController : UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        self.searchResults = heroesArray.filter({ (hero) -> Bool in
            
            let text = hero.name as NSString
            let result = text.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
            return result.location != NSNotFound
            
        })
        
        if searchResults.count == 0{
            self.searchActive = false
        }else{
            self.searchActive = true
        }
        
        self.heroTableView.reloadData()
        
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        self.searchActive = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.searchActive = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.searchActive = false
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchActive = false
    }
    
    
    
}
