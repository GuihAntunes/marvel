//
//  Event.swift
//  Marvel
//
//  Created by Guilherme Antunes on 25/06/17.
//  Copyright © 2017 Guihsoft. All rights reserved.
//

import Foundation
import Freddy

struct Event {
    
    var path : String = ""
    var ext : String = ""
    var images = [UIImage]()

    
}

extension Event : JSONDecodable {
    
    init(json value: JSON) throws {
        self.path = try value.getString(at: "thumbnail","path")
        self.ext = try value.getString(at: "thumbnail","extension")
    }
    
}

extension Event {
    
    static func saveEvents(images : [UIImage], heroName : String) {

        let qtdImages = images.count
        var eventsArray = [URL]()
        
        guard images.isEmpty == false else {
            return
        }
        
        for i in 0...(qtdImages - 1) {
            if let eventToSaveImageData = UIImagePNGRepresentation(images[i]) {
                let filePathImage = (pathDocuments as NSString).appendingPathComponent("\(heroName)\(i).png")
                let url = URL(fileURLWithPath: filePathImage)
                try? eventToSaveImageData.write(to: url)
                eventsArray.append(url)
            }
        }
        
        eventsSavedImages[heroName] = eventsArray
        
    }
    
    static func deleteEvents(heroName : String) {

        guard let urls = eventsSavedImages[heroName] else {
            return
        }
        guard let count = eventsSavedImages[heroName]?.count else {
            return
        }
        
        do {
            
            for i in 0...(count - 1) {
                try fileMan.removeItem(at: urls[i])
            }
            
            eventsSavedImages.removeValue(forKey: heroName)
            
        }catch{print("Error on deleting!")}
        
    }
    
}
