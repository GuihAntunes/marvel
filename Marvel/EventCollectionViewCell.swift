//
//  EventCollectionViewCell.swift
//  Marvel
//
//  Created by Guilherme Antunes on 25/06/17.
//  Copyright © 2017 Guihsoft. All rights reserved.
//

import UIKit

class EventCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var eventImage: UIImageView!
    
}
